<?php

class Cote {
	public static $id_count = 0; //счетчик для индификаторов
	private $all_milk;
	private $all_edges;
	private $all_animals = array(); 

	private function new_id(){  //возвращает уникальный индификатор каждому животному
		$buff = self::$id_count;
		self::$id_count++;
		return $buff;		
	}

	public function add_cow($count){ // Добавляет нужное количество коров
		for ($i = 0; $i < $count; $i++){
			$this->all_animals[] = new Cow($this->new_id());//Пишем коров в общий массив
		}
	
	
	}

	public function add_chicken($count){ //Добавляет нужное количество куриц
		for ($i = 0; $i < $count; $i++){
			$this->all_animals[] = new Chicken($this->new_id()); //Собираем куриц в общий массив
		}	
	}

	public function get_product(){ //Получаем продукты за день
		foreach ($this->all_animals as &$animals){ 
			if($animals instanceof Cow){    // Отделяем молоко от яиц и все суммируем
				$this->all_milk += $animals->take_product();
			}
			if($animals instanceof Chicken){
				$this->all_edges += $animals->take_product();
			}
		}
	
	}
	
	public function print_product(){ //Вывод на печать
		echo 'Milk - ' . $this->all_milk . PHP_EOL;;
		echo 'Edges - ' . $this->all_edges;
	}
	
	
	
}
?>